## Hotel Search Vue.JS Demo

This Hotel Search demo is based on Laravel 5.6 & Vue.js. It contains a very basic implemetnation of search loging, geo location, and a dynamic view to see hotels and their room availability as determined by the DB. 

This was inital done as a test for a job interview... It will more than likely not be developed beyond what is available here in the repo. 

A copy of the databse can be found in the db_backup folder in root
