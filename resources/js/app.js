require('./bootstrap');

window.Vue = require('vue');
window.VueRouter = require('vue-router');
window.Vue.use(window.VueRouter);

import VModal from 'vue-js-modal';
window.Vue.use(VModal)

Vue.component('hotel-search-view', require('./components/Hotels/SearchComponent.vue').default);

window.onload = function () {

	var app = new Vue({
	    el: '#app',
	    data: {
			settings : {
				api: {
					token: api_token,
					url: api_url,
				},
				debug: false,
			},
	    }
	});

}
