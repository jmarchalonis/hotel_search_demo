export default {
    general: {
        tooltip: "always",
        tooltipDir: [
          "bottom",
          "bottom"
        ],
        height: 8,
        lazy: true,
        dotSize: 20,
        piecewise: false,
        style: {
          "marginBottom": "30px"
        },
        bgStyle: {
          "backgroundColor": "#fff",
          "boxShadow": "inset 0.5px 0.5px 3px 1px rgba(0,0,0,.36)"
        },
        sliderStyle: [
          {
            "backgroundColor": "#249BC7"
          },
          {
            "backgroundColor": "#4983B7"
          }
        ],
        tooltipStyle: [
          {
            "backgroundColor": "#249BC7",
            "borderColor": "#249BC7"
          },
          {
            "backgroundColor": "#4983B7",
            "borderColor": "#4983B7"
          }
        ],
        processStyle: {
          "backgroundImage": "-webkit-linear-gradient(left, #249BC7, #4983B7)"
        },
        piecewiseStyle: {
          "backgroundColor": "#ccc",
          "visibility": "visible",
          "width": "12px",
          "height": "12px"
        },
    },
    single : {
        tooltip: "hover",
        tooltipDir: "bottom",
        lazy: true,
        dotSize: 20,
        piecewise: true,
        piecewiseLabel: true,
        piecewiseStyle: {
          "backgroundColor": "#ccc",
          "visibility": "visible",
          "width": "12px",
          "height": "12px"
        },
        piecewiseActiveStyle: {
          "backgroundColor": "#249BC7"
        },
        labelActiveStyle: {
          "color": "#249BC7"
        }
    }
}