<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title><?php echo __('Hotel Search'); ?></title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        
        <script type="text/javascript">
		    var root = "/>";
		    var api_url = "";
		    var api_token = 'xyzabc';
		</script>

		<link href="/css/search-styles.css" rel="stylesheet">
		<link href="/css/spacing_helper.css" rel="stylesheet">
		<link href="/css/app.css" rel="stylesheet">

        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
  integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
  crossorigin=""/>
		
	</head>
	<body>
		
		<div class="container mt-15">
			<strong>
				<?php echo __('Hotel Search'); ?>
			</strong>

			<div id="app">
				<hotel-search-view v-bind:settings="settings"></hotel-search-view>
			</div>

		</div>

	</body>

	<script src="/js/app.js"></script>
	<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
		  integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
		  crossorigin=""></script>

</html>