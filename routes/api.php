<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Hotel Search Endpoints
// Endpoint is used to search hotel and room inventory
Route::get('/hotels/search.json', 'SearchController@doSearch');

// Endpoint is used to process a booking for a hotel room
Route::post('/hotels/processBooking.json', 'ProcessController@processBooking');

