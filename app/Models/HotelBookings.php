<?php
/**
  * Hotel Booking Table Class
  * This class contains functions related to hotel booking records
  * 
  * @author Jason marchalonis
  * @since 1.0
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelBookings extends Model
{
    
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public $timestamps = true;
    protected $table = 'hotel_bookings';
    protected $connection = 'mysql';

    /**
     * Save New Booking Method
     * This method is used to save a new booking to the database. 
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param array $data This is the passed data from the controller. 
     * @return bool bool This is the result of the record save.
    */
	public function saveNewBooking( $data = [] )
	{

		// Save The Booking Record
		$record = new $this;

		$record->status = 'paid';
		$record->room_id = $data['room_id'];
		$record->hotel_id = $data['hotel_id'];
		$record->first_name = $data['first_name'];
		$record->last_name = $data['last_name'];
		$record->email = $data['email'];
		$record->phone = $data['phone'];
		$record->address_line = $data['address_line'];
		$record->address_line_2 = $data['address_line_2'];
		$record->city = $data['city'];
		$record->state = $data['state'];
		$record->zip = $data['zip'];
		$record->cc_number = $data['cc_number'];
		$record->cc_exp_month = $data['cc_exp_month'];
		$record->cc_exp_year = $data['cc_exp_year'];
		$record->price = $data['price'];
		$record->taxes = $data['taxes'];
		$record->fees = $data['fees'];
		$record->total = $data['total'];

		$record->save();

		return true;
	}

}