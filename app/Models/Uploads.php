<?php
/**
  * Uploads Table Class
  * This class contains functions related to the uploads
  * 
  * @author Jason marchalonis
  * @since 1.0
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Uploads extends Model
{
    
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public $timestamps = true;
    protected $table = 'uploads';
    protected $connection = 'mysql';

}