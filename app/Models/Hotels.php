<?php
/**
  * Hotel Table Class
  * This class contains functions relatedto hotel records
  * 
  * @author Jason marchalonis
  * @since 1.0
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Hotels extends Model
{
    
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public $timestamps = true;
    protected $table = 'hotels';
    protected $connection = 'mysql';

    /**
     * Uploads Method
     * This method is used to a hotel's upload via ORM. In this case, the hotel image. 
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param int $id This id of the hotel to get records for
     * @return array $records The result containing a hotel's uploads
    */
    public function uploads( $id = null )
    {

    	$records = [];
    	$query = $this->hasMany('App\Models\Uploads', 'foreign_key', 'id' )->where([
    		[ 'model','=', 'Hotels' ],
    	])->get();

        if( !empty( $query ) ){
            foreach ($query as $key => $record ) {
            	$records[] = $record->getAttributes();
            }
        }

        return $records;
	}

    /**
     * Get Geo Location Method
     * This method is used to a hotel's geo location records. It is used to map the locations to a map.
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @return array $record The result containing the hotel's location
    */
	public function getGeoLocation( )
    {
    	$record = $this->hasOne('App\Models\HotelGeoLocations', 'hotel_id', 'id' )->get();

    	if( !empty($record) ){
    		return $record[0]->getAttributes();
    	}
        return $record;
	}

    /**
     * Get Hotel Room List Method
     * This method is used to return a list of hotel rooms for a giving hotel id
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @return array $records The result containing the hotel's rooms
    */
	public function getHotelRoomList( )
    {
    	$records = $this->hasOne('App\Models\HotelRooms', 'hotel_id', 'id' )->orderBy('price', 'asc')->get()->toArray();
        return $records;
	}


    /**
     * Get Hotel Records Method
     * This method is used to return a paginated list of hotels from the database.
     * It is returned as JSON via the APU
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param int $limit This number of records to return
     * @param array $condtions The records condtions
     * @return array $records The result containing the hotel's rooms
    */
	public function getHotelRecords( $limit = 15, $condtions = [] )
    {

		$records = [];
		$record_count = 0;

		if( isset( $condtions['keyword'] ) && !empty( $condtions['keyword'] ) ) {
			$query = $this->where('name', 'like', '%' . $condtions['keyword'] . '%' )->paginate( $limit );
		} else {
			$query = $this->paginate( $limit );
		}

        //  If records are found, we call and format the contained records here
		if( !empty( $query ) ){
			foreach ($query as $key => $record ) {

            	$records['records'][$key] = $record->getAttributes();
            	$records['records'][$key]['image'] = $record->uploads( $record->getAttributes()['id'] );
            	$records['records'][$key]['location'] = $record->getGeoLocation( );
            	   
            	$rooms = $record->getHotelRoomList();
            	$records['records'][$key]['rooms'] = $rooms;
            	$records['records'][$key]['lowest_price'] = 'N/A';
            	if( !empty($rooms) ){
            		$records['records'][$key]['lowest_price'] = $rooms[0]['price'];
            	}

            }

            $record_count = count( $query );
		}

        //  We format the pagination response string here for processing on the front-end.
		$records['pagination'] = [
    		'perPage' => $query->perPage(),
    		'currentPage' => $query->currentPage(),
    		'total' => $query->total(),
    		'lastPage' => $query->lastPage(),
    		'hasNext' => false,
    		'hasPrev' => false,
    		'count' => $record_count,
    	];

    	if( $query->currentPage() < $query->total() ){
    		$records['pagination']['hasNext'] = true;
    	}

    	if( $query->currentPage() > 1 && $query->currentPage() <= $query->total() ){
    		$records['pagination']['hasPrev'] = true;
    	}

		return  $records;
	}

}