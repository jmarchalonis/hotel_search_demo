<?php
/**
  * Hotel Rooms Class
  * This class contains functions related to hotel room records
  * 
  * @author Jason marchalonis
  * @since 1.0
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelRooms extends Model
{
    
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public $timestamps = true;
    protected $table = 'hotel_rooms';
    protected $connection = 'mysql';

    /**
     * Upodate Room Status Method
     * This method is used to update a hotel rooms status once a booking has taken place. 
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param int $room_id This id of the room to update
     * @return bool bool This is the result of the record save.
    */
    public function updateRoomStatus( $room_id = NULL )
    {

    	$record = $this::find( $room_id );
    	$record->status = 'On Request';
    	$record->save();

    	return true;
    }

}