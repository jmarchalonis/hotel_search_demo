<?php
/**
  * Geo Location Table Class
  * This class contains functions related to geo location records.
  * 
  * @author Jason marchalonis
  * @since 1.0
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelGeoLocations extends Model
{
    
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    public $timestamps = true;
    protected $table = 'hotel_geo_locations';
    protected $connection = 'mysql';

}