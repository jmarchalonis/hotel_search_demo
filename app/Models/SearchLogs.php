<?php
/**
  * Save Logs Table Class
  * This class contains functions related to search logs
  * 
  * @author Jason marchalonis
  * @since 1.0
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchLogs extends Model
{
    
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';


    public $timestamps = true;
    protected $table = 'search_logs';
    protected $connection = 'mysql';

    /**
     * Save Log Method
     * This method is used to save a new search log to the database. 
     * As this is a demo, this function is very simple and limited in scope.
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param string $keyword This keyword that the use searched or was looking for.
     * @return bool bool This is the result of the record save.
    */
    public function saveLog( $keyword = '' )
    {

    	$record = new $this;
    	$record->keyword = $keyword;
    	$record->save();

    	return true;
    }

}