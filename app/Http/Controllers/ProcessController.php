<?php
/**
  * Process Controller Class
  * This class contains functionality for handling the process of a boooking via API
  * 
  * @author Jason marchalonis
  * @since 1.0
*/
namespace App\Http\Controllers;

use App\User;
use App\Models\HotelBookings;
use App\Models\HotelRooms;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProcessController extends AppController
{
    
    /**
     * Class Construct Method
     * This method is used to load common tables and processes
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @return void
    */
    public function __construct()
    {   
        //  Load our general common method for this class. 
        $this->HotelBookings = new HotelBookings();
        $this->HotelRooms = new HotelRooms();
    }

    /**
     * Process Booking Method
     * This method is used to process a posted booking and updates 
     * the room availabilty based onthe reuslt. 
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param Request $request This is an object of passed request data from the core
     * @return json The json response data of the process.
    */
    public function processBooking(Request $request)
    {
        
        $processed = $this->HotelBookings->saveNewBooking( $request->all() );
        
        if( $processed == true ){
            // Update The Room Status
            $this->HotelRooms->updateRoomStatus( $request->all()['room_id'] );

        }

        return response()->json(  [ 'success' => true ] );
    }

}