<?php
/**
  * Search Controller Class
  * This class contains functionality for handling hotel searching
  * 
  * @author Jason marchalonis
  * @since 1.0
*/
namespace App\Http\Controllers;

use App\User;
use App\Models\Hotels;
use App\Models\SearchLogs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchController extends AppController
{
    
    /**
     * Class Construct Method
     * This method is used to load common tables and processes
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @return void
    */
    public function __construct()
    {
       $this->Hotels = new Hotels();
       $this->SearchLogs = new SearchLogs();
    }   

    /**
     * Do Search Method
     * This method is used to handle API request for hotel search requests and
     * the room availabilty based on the reuslt. It also logs search result to later be used
     * as a method to learn user insights.
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param Request $request This is an object of passed request data from the core
     * @return json The json response data of the process.
    */
    public function doSearch(Request $request)
    {
        $keyword = $request->input('keyword', NULL);

        // We save the user search parameters here
        // We Only save a record if the keyword is not empty here and greater than 3 chars
        if( !empty( $keyword ) && strlen($keyword) > 3 ){
            $this->SearchLogs->saveLog( $keyword );
        }

        $records = $this->Hotels->getHotelRecords( 15, [ 'keyword' => $keyword ] );
        return response()->json(  $records );
    }

    /**
     * Process Booking Method
     * This method is used to handle API post for processing a booking request.
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param Request $request This is an object of passed request data from the core
     * @return json The json response data of the process.
    */
    public function processBooking(Request $request)
    {

        $this->debug( $request->all() );
        return response()->json(  true );

    }

    /**
     * Index Static Method
     * This is a static method for showing an index method
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @return view The static index view
    */
    public function index( )
    {
        return view('search.index', [ ] );
    }

}