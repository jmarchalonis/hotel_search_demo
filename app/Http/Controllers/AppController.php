<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AppController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Debug Function
     * This function is used to display a debug/string out to the user to test arrays, objects and strings.
     *
     * @author Jason Marchalonis
     * @since 1.0
     * @param mixed $mixed This is the passed data... can be array, int, string, object
     * @return json $response The formated response containing the result
    */
    public function debug( $mixed = '' ){

    	echo '<pre>';
    		print_r($mixed);
    	echo '</pre>';
    }

}
